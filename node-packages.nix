# This file has been generated by node2nix 1.1.1. Do not edit!

{nodeEnv, fetchurl, fetchgit, globalBuildInputs ? []}:

let
  sources = {
    "absolute-0.0.1" = {
      name = "absolute";
      packageName = "absolute";
      version = "0.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/absolute/-/absolute-0.0.1.tgz";
        sha1 = "c22822f87e1c939f579887504d9c109c4173829d";
      };
    };
    "chalk-1.1.3" = {
      name = "chalk";
      packageName = "chalk";
      version = "1.1.3";
      src = fetchurl {
        url = "https://registry.npmjs.org/chalk/-/chalk-1.1.3.tgz";
        sha1 = "a8115c55e4a702fe4d150abd3872822a7e09fc98";
      };
    };
    "clone-1.0.2" = {
      name = "clone";
      packageName = "clone";
      version = "1.0.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/clone/-/clone-1.0.2.tgz";
        sha1 = "260b7a99ebb1edfe247538175f783243cb19d149";
      };
    };
    "co-fs-extra-1.2.1" = {
      name = "co-fs-extra";
      packageName = "co-fs-extra";
      version = "1.2.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/co-fs-extra/-/co-fs-extra-1.2.1.tgz";
        sha1 = "3b6ad77cf2614530f677b1cf62664f5ba756b722";
      };
    };
    "commander-2.9.0" = {
      name = "commander";
      packageName = "commander";
      version = "2.9.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/commander/-/commander-2.9.0.tgz";
        sha1 = "9c99094176e12240cb22d6c5146098400fe0f7d4";
      };
    };
    "gray-matter-2.1.1" = {
      name = "gray-matter";
      packageName = "gray-matter";
      version = "2.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/gray-matter/-/gray-matter-2.1.1.tgz";
        sha1 = "3042d9adec2a1ded6a7707a9ed2380f8a17a430e";
      };
    };
    "has-generators-1.0.1" = {
      name = "has-generators";
      packageName = "has-generators";
      version = "1.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/has-generators/-/has-generators-1.0.1.tgz";
        sha1 = "a6a2e55486011940482e13e2c93791c449acf449";
      };
    };
    "is-3.2.1" = {
      name = "is";
      packageName = "is";
      version = "3.2.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/is/-/is-3.2.1.tgz";
        sha1 = "d0ac2ad55eb7b0bec926a5266f6c662aaa83dca5";
      };
    };
    "is-utf8-0.2.1" = {
      name = "is-utf8";
      packageName = "is-utf8";
      version = "0.2.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/is-utf8/-/is-utf8-0.2.1.tgz";
        sha1 = "4b0da1442104d1b336340e80797e865cf39f7d72";
      };
    };
    "recursive-readdir-2.1.1" = {
      name = "recursive-readdir";
      packageName = "recursive-readdir";
      version = "2.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/recursive-readdir/-/recursive-readdir-2.1.1.tgz";
        sha1 = "a01cfc7f7f38a53ec096a096f63a50489c3e297c";
      };
    };
    "rimraf-2.6.1" = {
      name = "rimraf";
      packageName = "rimraf";
      version = "2.6.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/rimraf/-/rimraf-2.6.1.tgz";
        sha1 = "c2338ec643df7a1b7fe5c54fa86f57428a55f33d";
      };
    };
    "stat-mode-0.2.2" = {
      name = "stat-mode";
      packageName = "stat-mode";
      version = "0.2.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/stat-mode/-/stat-mode-0.2.2.tgz";
        sha1 = "e6c80b623123d7d80cf132ce538f346289072502";
      };
    };
    "thunkify-2.1.2" = {
      name = "thunkify";
      packageName = "thunkify";
      version = "2.1.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/thunkify/-/thunkify-2.1.2.tgz";
        sha1 = "faa0e9d230c51acc95ca13a361ac05ca7e04553d";
      };
    };
    "unyield-0.0.1" = {
      name = "unyield";
      packageName = "unyield";
      version = "0.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/unyield/-/unyield-0.0.1.tgz";
        sha1 = "150e65da42bf7742445b958a64eb9b85d1d2b180";
      };
    };
    "ware-1.3.0" = {
      name = "ware";
      packageName = "ware";
      version = "1.3.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/ware/-/ware-1.3.0.tgz";
        sha1 = "d1b14f39d2e2cb4ab8c4098f756fe4b164e473d4";
      };
    };
    "win-fork-1.1.1" = {
      name = "win-fork";
      packageName = "win-fork";
      version = "1.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/win-fork/-/win-fork-1.1.1.tgz";
        sha1 = "8f58e0656fca00adc8c86a2b89e3cd2d6a2d5e5e";
      };
    };
    "ansi-styles-2.2.1" = {
      name = "ansi-styles";
      packageName = "ansi-styles";
      version = "2.2.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/ansi-styles/-/ansi-styles-2.2.1.tgz";
        sha1 = "b432dd3358b634cf75e1e4664368240533c1ddbe";
      };
    };
    "escape-string-regexp-1.0.5" = {
      name = "escape-string-regexp";
      packageName = "escape-string-regexp";
      version = "1.0.5";
      src = fetchurl {
        url = "https://registry.npmjs.org/escape-string-regexp/-/escape-string-regexp-1.0.5.tgz";
        sha1 = "1b61c0562190a8dff6ae3bb2cf0200ca130b86d4";
      };
    };
    "has-ansi-2.0.0" = {
      name = "has-ansi";
      packageName = "has-ansi";
      version = "2.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/has-ansi/-/has-ansi-2.0.0.tgz";
        sha1 = "34f5049ce1ecdf2b0649af3ef24e45ed35416d91";
      };
    };
    "strip-ansi-3.0.1" = {
      name = "strip-ansi";
      packageName = "strip-ansi";
      version = "3.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/strip-ansi/-/strip-ansi-3.0.1.tgz";
        sha1 = "6a385fb8853d952d5ff05d0e8aaf94278dc63dcf";
      };
    };
    "supports-color-2.0.0" = {
      name = "supports-color";
      packageName = "supports-color";
      version = "2.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/supports-color/-/supports-color-2.0.0.tgz";
        sha1 = "535d045ce6b6363fa40117084629995e9df324c7";
      };
    };
    "ansi-regex-2.1.1" = {
      name = "ansi-regex";
      packageName = "ansi-regex";
      version = "2.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/ansi-regex/-/ansi-regex-2.1.1.tgz";
        sha1 = "c3b33ab5ee360d86e0e628f0468ae7ef27d654df";
      };
    };
    "co-from-stream-0.0.0" = {
      name = "co-from-stream";
      packageName = "co-from-stream";
      version = "0.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/co-from-stream/-/co-from-stream-0.0.0.tgz";
        sha1 = "1a5cd8ced77263946094fa39f2499a63297bcaf9";
      };
    };
    "fs-extra-0.26.7" = {
      name = "fs-extra";
      packageName = "fs-extra";
      version = "0.26.7";
      src = fetchurl {
        url = "https://registry.npmjs.org/fs-extra/-/fs-extra-0.26.7.tgz";
        sha1 = "9ae1fdd94897798edab76d0918cf42d0c3184fa9";
      };
    };
    "thunkify-wrap-1.0.4" = {
      name = "thunkify-wrap";
      packageName = "thunkify-wrap";
      version = "1.0.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/thunkify-wrap/-/thunkify-wrap-1.0.4.tgz";
        sha1 = "b52be548ddfefda20e00b58c6096762b43dd6880";
      };
    };
    "co-read-0.0.1" = {
      name = "co-read";
      packageName = "co-read";
      version = "0.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/co-read/-/co-read-0.0.1.tgz";
        sha1 = "f81b3eb8a86675fec51e3d883a7f564e873c9389";
      };
    };
    "graceful-fs-4.1.11" = {
      name = "graceful-fs";
      packageName = "graceful-fs";
      version = "4.1.11";
      src = fetchurl {
        url = "https://registry.npmjs.org/graceful-fs/-/graceful-fs-4.1.11.tgz";
        sha1 = "0e8bdfe4d1ddb8854d64e04ea7c00e2a026e5658";
      };
    };
    "jsonfile-2.4.0" = {
      name = "jsonfile";
      packageName = "jsonfile";
      version = "2.4.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/jsonfile/-/jsonfile-2.4.0.tgz";
        sha1 = "3736a2b428b87bbda0cc83b53fa3d633a35c2ae8";
      };
    };
    "klaw-1.3.1" = {
      name = "klaw";
      packageName = "klaw";
      version = "1.3.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/klaw/-/klaw-1.3.1.tgz";
        sha1 = "4088433b46b3b1ba259d78785d8e96f73ba02439";
      };
    };
    "path-is-absolute-1.0.1" = {
      name = "path-is-absolute";
      packageName = "path-is-absolute";
      version = "1.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/path-is-absolute/-/path-is-absolute-1.0.1.tgz";
        sha1 = "174b9268735534ffbc7ace6bf53a5a9e1b5c5f5f";
      };
    };
    "enable-1.3.2" = {
      name = "enable";
      packageName = "enable";
      version = "1.3.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/enable/-/enable-1.3.2.tgz";
        sha1 = "9eba6837d16d0982b59f87d889bf754443d52931";
      };
    };
    "graceful-readlink-1.0.1" = {
      name = "graceful-readlink";
      packageName = "graceful-readlink";
      version = "1.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/graceful-readlink/-/graceful-readlink-1.0.1.tgz";
        sha1 = "4cafad76bc62f02fa039b2f94e9a3dd3a391a725";
      };
    };
    "ansi-red-0.1.1" = {
      name = "ansi-red";
      packageName = "ansi-red";
      version = "0.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/ansi-red/-/ansi-red-0.1.1.tgz";
        sha1 = "8c638f9d1080800a353c9c28c8a81ca4705d946c";
      };
    };
    "coffee-script-1.12.4" = {
      name = "coffee-script";
      packageName = "coffee-script";
      version = "1.12.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/coffee-script/-/coffee-script-1.12.4.tgz";
        sha1 = "fe1bced97fe1fb3927b998f2b45616e0658be1ff";
      };
    };
    "extend-shallow-2.0.1" = {
      name = "extend-shallow";
      packageName = "extend-shallow";
      version = "2.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/extend-shallow/-/extend-shallow-2.0.1.tgz";
        sha1 = "51af7d614ad9a9f610ea1bafbb989d6b1c56890f";
      };
    };
    "js-yaml-3.8.1" = {
      name = "js-yaml";
      packageName = "js-yaml";
      version = "3.8.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/js-yaml/-/js-yaml-3.8.1.tgz";
        sha1 = "782ba50200be7b9e5a8537001b7804db3ad02628";
      };
    };
    "toml-2.3.2" = {
      name = "toml";
      packageName = "toml";
      version = "2.3.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/toml/-/toml-2.3.2.tgz";
        sha1 = "5eded5ca42887924949fd06eb0e955656001e834";
      };
    };
    "ansi-wrap-0.1.0" = {
      name = "ansi-wrap";
      packageName = "ansi-wrap";
      version = "0.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/ansi-wrap/-/ansi-wrap-0.1.0.tgz";
        sha1 = "a82250ddb0015e9a27ca82e82ea603bbfa45efaf";
      };
    };
    "is-extendable-0.1.1" = {
      name = "is-extendable";
      packageName = "is-extendable";
      version = "0.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/is-extendable/-/is-extendable-0.1.1.tgz";
        sha1 = "62b110e289a471418e3ec36a617d472e301dfc89";
      };
    };
    "argparse-1.0.9" = {
      name = "argparse";
      packageName = "argparse";
      version = "1.0.9";
      src = fetchurl {
        url = "https://registry.npmjs.org/argparse/-/argparse-1.0.9.tgz";
        sha1 = "73d83bc263f86e97f8cc4f6bae1b0e90a7d22c86";
      };
    };
    "esprima-3.1.3" = {
      name = "esprima";
      packageName = "esprima";
      version = "3.1.3";
      src = fetchurl {
        url = "https://registry.npmjs.org/esprima/-/esprima-3.1.3.tgz";
        sha1 = "fdca51cee6133895e3c88d535ce49dbff62a4633";
      };
    };
    "sprintf-js-1.0.3" = {
      name = "sprintf-js";
      packageName = "sprintf-js";
      version = "1.0.3";
      src = fetchurl {
        url = "https://registry.npmjs.org/sprintf-js/-/sprintf-js-1.0.3.tgz";
        sha1 = "04e6926f662895354f3dd015203633b857297e2c";
      };
    };
    "minimatch-3.0.3" = {
      name = "minimatch";
      packageName = "minimatch";
      version = "3.0.3";
      src = fetchurl {
        url = "https://registry.npmjs.org/minimatch/-/minimatch-3.0.3.tgz";
        sha1 = "2a4e4090b96b2db06a9d7df01055a62a77c9b774";
      };
    };
    "brace-expansion-1.1.6" = {
      name = "brace-expansion";
      packageName = "brace-expansion";
      version = "1.1.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/brace-expansion/-/brace-expansion-1.1.6.tgz";
        sha1 = "7197d7eaa9b87e648390ea61fc66c84427420df9";
      };
    };
    "balanced-match-0.4.2" = {
      name = "balanced-match";
      packageName = "balanced-match";
      version = "0.4.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/balanced-match/-/balanced-match-0.4.2.tgz";
        sha1 = "cb3f3e3c732dc0f01ee70b403f302e61d7709838";
      };
    };
    "concat-map-0.0.1" = {
      name = "concat-map";
      packageName = "concat-map";
      version = "0.0.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/concat-map/-/concat-map-0.0.1.tgz";
        sha1 = "d8a96bd77fd68df7793a73036a3ba0d5405d477b";
      };
    };
    "glob-7.1.1" = {
      name = "glob";
      packageName = "glob";
      version = "7.1.1";
      src = fetchurl {
        url = "https://registry.npmjs.org/glob/-/glob-7.1.1.tgz";
        sha1 = "805211df04faaf1c63a3600306cdf5ade50b2ec8";
      };
    };
    "fs.realpath-1.0.0" = {
      name = "fs.realpath";
      packageName = "fs.realpath";
      version = "1.0.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/fs.realpath/-/fs.realpath-1.0.0.tgz";
        sha1 = "1504ad2523158caa40db4a2787cb01411994ea4f";
      };
    };
    "inflight-1.0.6" = {
      name = "inflight";
      packageName = "inflight";
      version = "1.0.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/inflight/-/inflight-1.0.6.tgz";
        sha1 = "49bd6331d7d02d0c09bc910a1075ba8165b56df9";
      };
    };
    "inherits-2.0.3" = {
      name = "inherits";
      packageName = "inherits";
      version = "2.0.3";
      src = fetchurl {
        url = "https://registry.npmjs.org/inherits/-/inherits-2.0.3.tgz";
        sha1 = "633c2c83e3da42a502f52466022480f4208261de";
      };
    };
    "once-1.4.0" = {
      name = "once";
      packageName = "once";
      version = "1.4.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/once/-/once-1.4.0.tgz";
        sha1 = "583b1aa775961d4b113ac17d9c50baef9dd76bd1";
      };
    };
    "wrappy-1.0.2" = {
      name = "wrappy";
      packageName = "wrappy";
      version = "1.0.2";
      src = fetchurl {
        url = "https://registry.npmjs.org/wrappy/-/wrappy-1.0.2.tgz";
        sha1 = "b5243d8f3ec1aa35f1364605bc0d1036e30ab69f";
      };
    };
    "co-3.1.0" = {
      name = "co";
      packageName = "co";
      version = "3.1.0";
      src = fetchurl {
        url = "https://registry.npmjs.org/co/-/co-3.1.0.tgz";
        sha1 = "4ea54ea5a08938153185e15210c68d9092bc1b78";
      };
    };
    "wrap-fn-0.1.5" = {
      name = "wrap-fn";
      packageName = "wrap-fn";
      version = "0.1.5";
      src = fetchurl {
        url = "https://registry.npmjs.org/wrap-fn/-/wrap-fn-0.1.5.tgz";
        sha1 = "f21b6e41016ff4a7e31720dbc63a09016bdf9845";
      };
    };
    "marked-0.3.6" = {
      name = "marked";
      packageName = "marked";
      version = "0.3.6";
      src = fetchurl {
        url = "https://registry.npmjs.org/marked/-/marked-0.3.6.tgz";
        sha1 = "b2c6c618fccece4ef86c4fc6cb8a7cbf5aeda8d7";
      };
    };
    "debug-0.7.4" = {
      name = "debug";
      packageName = "debug";
      version = "0.7.4";
      src = fetchurl {
        url = "https://registry.npmjs.org/debug/-/debug-0.7.4.tgz";
        sha1 = "06e1ea8082c2cb14e39806e22e2f6f757f92af39";
      };
    };
  };
in
{
  metalsmith = nodeEnv.buildNodePackage {
    name = "metalsmith";
    packageName = "metalsmith";
    version = "2.3.0";
    src = fetchurl {
      url = "https://registry.npmjs.org/metalsmith/-/metalsmith-2.3.0.tgz";
      sha1 = "833afbb5a2a6385e2d9ae3d935e39e33eaea5231";
    };
    dependencies = [
      sources."absolute-0.0.1"
      (sources."chalk-1.1.3" // {
        dependencies = [
          sources."ansi-styles-2.2.1"
          sources."escape-string-regexp-1.0.5"
          (sources."has-ansi-2.0.0" // {
            dependencies = [
              sources."ansi-regex-2.1.1"
            ];
          })
          (sources."strip-ansi-3.0.1" // {
            dependencies = [
              sources."ansi-regex-2.1.1"
            ];
          })
          sources."supports-color-2.0.0"
        ];
      })
      sources."clone-1.0.2"
      (sources."co-fs-extra-1.2.1" // {
        dependencies = [
          (sources."co-from-stream-0.0.0" // {
            dependencies = [
              sources."co-read-0.0.1"
            ];
          })
          (sources."fs-extra-0.26.7" // {
            dependencies = [
              sources."graceful-fs-4.1.11"
              sources."jsonfile-2.4.0"
              sources."klaw-1.3.1"
              sources."path-is-absolute-1.0.1"
            ];
          })
          (sources."thunkify-wrap-1.0.4" // {
            dependencies = [
              sources."enable-1.3.2"
            ];
          })
        ];
      })
      (sources."commander-2.9.0" // {
        dependencies = [
          sources."graceful-readlink-1.0.1"
        ];
      })
      (sources."gray-matter-2.1.1" // {
        dependencies = [
          (sources."ansi-red-0.1.1" // {
            dependencies = [
              sources."ansi-wrap-0.1.0"
            ];
          })
          sources."coffee-script-1.12.4"
          (sources."extend-shallow-2.0.1" // {
            dependencies = [
              sources."is-extendable-0.1.1"
            ];
          })
          (sources."js-yaml-3.8.1" // {
            dependencies = [
              (sources."argparse-1.0.9" // {
                dependencies = [
                  sources."sprintf-js-1.0.3"
                ];
              })
              sources."esprima-3.1.3"
            ];
          })
          sources."toml-2.3.2"
        ];
      })
      sources."has-generators-1.0.1"
      sources."is-3.2.1"
      sources."is-utf8-0.2.1"
      (sources."recursive-readdir-2.1.1" // {
        dependencies = [
          (sources."minimatch-3.0.3" // {
            dependencies = [
              (sources."brace-expansion-1.1.6" // {
                dependencies = [
                  sources."balanced-match-0.4.2"
                  sources."concat-map-0.0.1"
                ];
              })
            ];
          })
        ];
      })
      (sources."rimraf-2.6.1" // {
        dependencies = [
          (sources."glob-7.1.1" // {
            dependencies = [
              sources."fs.realpath-1.0.0"
              (sources."inflight-1.0.6" // {
                dependencies = [
                  sources."wrappy-1.0.2"
                ];
              })
              sources."inherits-2.0.3"
              (sources."minimatch-3.0.3" // {
                dependencies = [
                  (sources."brace-expansion-1.1.6" // {
                    dependencies = [
                      sources."balanced-match-0.4.2"
                      sources."concat-map-0.0.1"
                    ];
                  })
                ];
              })
              (sources."once-1.4.0" // {
                dependencies = [
                  sources."wrappy-1.0.2"
                ];
              })
              sources."path-is-absolute-1.0.1"
            ];
          })
        ];
      })
      sources."stat-mode-0.2.2"
      sources."thunkify-2.1.2"
      (sources."unyield-0.0.1" // {
        dependencies = [
          sources."co-3.1.0"
        ];
      })
      (sources."ware-1.3.0" // {
        dependencies = [
          (sources."wrap-fn-0.1.5" // {
            dependencies = [
              sources."co-3.1.0"
            ];
          })
        ];
      })
      sources."win-fork-1.1.1"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "An extremely simple, pluggable static site generator.";
      homepage = "https://github.com/segmentio/metalsmith#readme";
      license = "MIT";
    };
    production = true;
  };
  metalsmith-markdown = nodeEnv.buildNodePackage {
    name = "metalsmith-markdown";
    packageName = "metalsmith-markdown";
    version = "0.2.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/metalsmith-markdown/-/metalsmith-markdown-0.2.1.tgz";
      sha1 = "1a1feeb6c76d15407b75271da81509d07932d753";
    };
    dependencies = [
      sources."marked-0.3.6"
      sources."debug-0.7.4"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "A Metalsmith plugin to convert markdown files.";
      homepage = https://github.com/segmentio/metalsmith-markdown;
      license = "MIT";
    };
    production = true;
  };
  absolute = nodeEnv.buildNodePackage {
    name = "absolute";
    packageName = "absolute";
    version = "0.0.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/absolute/-/absolute-0.0.1.tgz";
      sha1 = "c22822f87e1c939f579887504d9c109c4173829d";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Test if a path (string) is absolute";
      homepage = https://github.com/bahamas10/node-absolute;
      license = "MIT";
    };
    production = true;
  };
  chalk = nodeEnv.buildNodePackage {
    name = "chalk";
    packageName = "chalk";
    version = "1.1.3";
    src = fetchurl {
      url = "https://registry.npmjs.org/chalk/-/chalk-1.1.3.tgz";
      sha1 = "a8115c55e4a702fe4d150abd3872822a7e09fc98";
    };
    dependencies = [
      sources."ansi-styles-2.2.1"
      sources."escape-string-regexp-1.0.5"
      (sources."has-ansi-2.0.0" // {
        dependencies = [
          sources."ansi-regex-2.1.1"
        ];
      })
      (sources."strip-ansi-3.0.1" // {
        dependencies = [
          sources."ansi-regex-2.1.1"
        ];
      })
      sources."supports-color-2.0.0"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "Terminal string styling done right. Much color.";
      homepage = "https://github.com/chalk/chalk#readme";
      license = "MIT";
    };
    production = true;
  };
  clone = nodeEnv.buildNodePackage {
    name = "clone";
    packageName = "clone";
    version = "2.1.0";
    src = fetchurl {
      url = "https://registry.npmjs.org/clone/-/clone-2.1.0.tgz";
      sha1 = "9c715bfbd39aa197c8ee0f8e65c3912ba34f8cd6";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "deep cloning of objects and arrays";
      homepage = "https://github.com/pvorb/node-clone#readme";
      license = "MIT";
    };
    production = true;
  };
  co-fs-extra = nodeEnv.buildNodePackage {
    name = "co-fs-extra";
    packageName = "co-fs-extra";
    version = "1.2.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/co-fs-extra/-/co-fs-extra-1.2.1.tgz";
      sha1 = "3b6ad77cf2614530f677b1cf62664f5ba756b722";
    };
    dependencies = [
      (sources."co-from-stream-0.0.0" // {
        dependencies = [
          sources."co-read-0.0.1"
        ];
      })
      (sources."fs-extra-0.26.7" // {
        dependencies = [
          sources."graceful-fs-4.1.11"
          sources."jsonfile-2.4.0"
          sources."klaw-1.3.1"
          sources."path-is-absolute-1.0.1"
          (sources."rimraf-2.6.1" // {
            dependencies = [
              (sources."glob-7.1.1" // {
                dependencies = [
                  sources."fs.realpath-1.0.0"
                  (sources."inflight-1.0.6" // {
                    dependencies = [
                      sources."wrappy-1.0.2"
                    ];
                  })
                  sources."inherits-2.0.3"
                  (sources."minimatch-3.0.3" // {
                    dependencies = [
                      (sources."brace-expansion-1.1.6" // {
                        dependencies = [
                          sources."balanced-match-0.4.2"
                          sources."concat-map-0.0.1"
                        ];
                      })
                    ];
                  })
                  (sources."once-1.4.0" // {
                    dependencies = [
                      sources."wrappy-1.0.2"
                    ];
                  })
                ];
              })
            ];
          })
        ];
      })
      (sources."thunkify-wrap-1.0.4" // {
        dependencies = [
          sources."enable-1.3.2"
        ];
      })
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "fs-extra for 'co'";
      homepage = "https://github.com/GKuChan/co-fs-extra#readme";
      license = "MIT";
    };
    production = true;
  };
  commander = nodeEnv.buildNodePackage {
    name = "commander";
    packageName = "commander";
    version = "2.9.0";
    src = fetchurl {
      url = "https://registry.npmjs.org/commander/-/commander-2.9.0.tgz";
      sha1 = "9c99094176e12240cb22d6c5146098400fe0f7d4";
    };
    dependencies = [
      sources."graceful-readlink-1.0.1"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "the complete solution for node.js command-line programs";
      homepage = "https://github.com/tj/commander.js#readme";
      license = "MIT";
    };
    production = true;
  };
  gray-matter = nodeEnv.buildNodePackage {
    name = "gray-matter";
    packageName = "gray-matter";
    version = "2.1.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/gray-matter/-/gray-matter-2.1.1.tgz";
      sha1 = "3042d9adec2a1ded6a7707a9ed2380f8a17a430e";
    };
    dependencies = [
      (sources."ansi-red-0.1.1" // {
        dependencies = [
          sources."ansi-wrap-0.1.0"
        ];
      })
      sources."coffee-script-1.12.4"
      (sources."extend-shallow-2.0.1" // {
        dependencies = [
          sources."is-extendable-0.1.1"
        ];
      })
      (sources."js-yaml-3.8.1" // {
        dependencies = [
          (sources."argparse-1.0.9" // {
            dependencies = [
              sources."sprintf-js-1.0.3"
            ];
          })
          sources."esprima-3.1.3"
        ];
      })
      sources."toml-2.3.2"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "Parse front-matter from a string or file. Fast, reliable and easy to use. Parses YAML front matter by default, but also has support for YAML, JSON, TOML or Coffee Front-Matter, with options to set custom delimiters. Used by metalsmith, assemble, verb and ";
      homepage = https://github.com/jonschlinkert/gray-matter;
      license = "MIT";
    };
    production = true;
  };
  has-generators = nodeEnv.buildNodePackage {
    name = "has-generators";
    packageName = "has-generators";
    version = "1.0.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/has-generators/-/has-generators-1.0.1.tgz";
      sha1 = "a6a2e55486011940482e13e2c93791c449acf449";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "feature-detect generators";
      homepage = https://gist.github.com/nathan7/53031bbfdf884ba2817a;
      license = "ISC";
    };
    production = true;
  };
  is = nodeEnv.buildNodePackage {
    name = "is";
    packageName = "is";
    version = "3.2.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/is/-/is-3.2.1.tgz";
      sha1 = "d0ac2ad55eb7b0bec926a5266f6c662aaa83dca5";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "the definitive JavaScript type testing library";
      homepage = https://github.com/enricomarino/is;
      license = "MIT";
    };
    production = true;
  };
  is-utf8 = nodeEnv.buildNodePackage {
    name = "is-utf8";
    packageName = "is-utf8";
    version = "0.2.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/is-utf8/-/is-utf8-0.2.1.tgz";
      sha1 = "4b0da1442104d1b336340e80797e865cf39f7d72";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Detect if a buffer is utf8 encoded.";
      homepage = "https://github.com/wayfind/is-utf8#readme";
      license = "MIT";
    };
    production = true;
  };
  recursive-readdir = nodeEnv.buildNodePackage {
    name = "recursive-readdir";
    packageName = "recursive-readdir";
    version = "2.1.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/recursive-readdir/-/recursive-readdir-2.1.1.tgz";
      sha1 = "a01cfc7f7f38a53ec096a096f63a50489c3e297c";
    };
    dependencies = [
      (sources."minimatch-3.0.3" // {
        dependencies = [
          (sources."brace-expansion-1.1.6" // {
            dependencies = [
              sources."balanced-match-0.4.2"
              sources."concat-map-0.0.1"
            ];
          })
        ];
      })
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "Get an array of all files in a directory and subdirectories.";
      homepage = "https://github.com/jergason/recursive-readdir#readme";
      license = "MIT";
    };
    production = true;
  };
  rimraf = nodeEnv.buildNodePackage {
    name = "rimraf";
    packageName = "rimraf";
    version = "2.6.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/rimraf/-/rimraf-2.6.1.tgz";
      sha1 = "c2338ec643df7a1b7fe5c54fa86f57428a55f33d";
    };
    dependencies = [
      (sources."glob-7.1.1" // {
        dependencies = [
          sources."fs.realpath-1.0.0"
          (sources."inflight-1.0.6" // {
            dependencies = [
              sources."wrappy-1.0.2"
            ];
          })
          sources."inherits-2.0.3"
          (sources."minimatch-3.0.3" // {
            dependencies = [
              (sources."brace-expansion-1.1.6" // {
                dependencies = [
                  sources."balanced-match-0.4.2"
                  sources."concat-map-0.0.1"
                ];
              })
            ];
          })
          (sources."once-1.4.0" // {
            dependencies = [
              sources."wrappy-1.0.2"
            ];
          })
          sources."path-is-absolute-1.0.1"
        ];
      })
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "A deep deletion module for node (like `rm -rf`)";
      homepage = "https://github.com/isaacs/rimraf#readme";
      license = "ISC";
    };
    production = true;
  };
  stat-mode = nodeEnv.buildNodePackage {
    name = "stat-mode";
    packageName = "stat-mode";
    version = "0.2.2";
    src = fetchurl {
      url = "https://registry.npmjs.org/stat-mode/-/stat-mode-0.2.2.tgz";
      sha1 = "e6c80b623123d7d80cf132ce538f346289072502";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Offers convenient getters and setters for the stat `mode`";
      homepage = https://github.com/TooTallNate/stat-mode;
      license = "MIT";
    };
    production = true;
  };
  thunkify = nodeEnv.buildNodePackage {
    name = "thunkify";
    packageName = "thunkify";
    version = "2.1.2";
    src = fetchurl {
      url = "https://registry.npmjs.org/thunkify/-/thunkify-2.1.2.tgz";
      sha1 = "faa0e9d230c51acc95ca13a361ac05ca7e04553d";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Turn callbacks, arrays, generators, generator functions, and promises into a thunk";
      homepage = https://github.com/visionmedia/node-thunkify;
      license = "MIT";
    };
    production = true;
  };
  unyield = nodeEnv.buildNodePackage {
    name = "unyield";
    packageName = "unyield";
    version = "0.0.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/unyield/-/unyield-0.0.1.tgz";
      sha1 = "150e65da42bf7742445b958a64eb9b85d1d2b180";
    };
    dependencies = [
      sources."co-3.1.0"
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "allow generators functions to accept callbacks";
      homepage = https://github.com/MatthewMueller/unyield;
    };
    production = true;
  };
  ware = nodeEnv.buildNodePackage {
    name = "ware";
    packageName = "ware";
    version = "1.3.0";
    src = fetchurl {
      url = "https://registry.npmjs.org/ware/-/ware-1.3.0.tgz";
      sha1 = "d1b14f39d2e2cb4ab8c4098f756fe4b164e473d4";
    };
    dependencies = [
      (sources."wrap-fn-0.1.5" // {
        dependencies = [
          sources."co-3.1.0"
        ];
      })
    ];
    buildInputs = globalBuildInputs;
    meta = {
      description = "Easily create your own middleware layer.";
      homepage = https://github.com/segmentio/ware;
      license = "MIT";
    };
    production = true;
  };
  win-fork = nodeEnv.buildNodePackage {
    name = "win-fork";
    packageName = "win-fork";
    version = "1.1.1";
    src = fetchurl {
      url = "https://registry.npmjs.org/win-fork/-/win-fork-1.1.1.tgz";
      sha1 = "8f58e0656fca00adc8c86a2b89e3cd2d6a2d5e5e";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Spawn for node.js but in a way that works regardless of which OS you're using";
      license = "BSD";
    };
    production = true;
  };
}