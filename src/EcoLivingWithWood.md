### Eco Living with Wood

#### Advantages of Using Wooden Kitchen Tools

First of all, lets look at the myth; wood harbours bacteria. Many studies
have been conducted on the subject.

One of the most famous studies, conducted at the University of California:
Davis, by Dean O. Cliver, Ph.D of the UC-Davis Food Safety Laboratory. His
research points out that there’s no significant antibacterial benefit from using
a plastic cutting board over a wood one. He notes that even if you apply
bacteria to a wooden cutting board, its natural properties cause the bacteria to
pass through the top layer of the wood and settle inside, where they’re very
difficult to bring out unless you split the board open.

Although the bacteria that have disappeared from the wood surfaces are found
alive inside the wood for some time after application, they evidently do not
multiply, and they gradually die. They can be detected only by splitting or
gouging the wood or by forcing water completely through from one surface to the
other. If a sharp knife is used to cut into the work surfaces after used plastic
or wood has been contaminated with bacteria and cleaned manually, more bacteria
are recovered from a used plastic surface than from a used wood surface.
Dr. Cliver’s study tested 10 different hardwoods and 4 different plastic
polymers. In the end, the result was a very scientific one: if you want a
plastic cutting board, anti-bacterial properties is no reason to buy one. If you
want a wooden cutting board, bacterial infection shouldn’t scare you away. It’s
more important that you take care to properly clean and disinfect whatever board
you buy,regardless of what it’s made of.

#### Other advantages include

#### Wooden Utensils won’t Scratch Your Cookware
Wooden utensils are soft and very gentle in use with the most delicate cooking
surfaces. One of the distinct advantages of wooden spatulas is that they are not
going to scratch the finish on your non-stick coatings. They also won’t scratch
your cast iron or stainless steel pots, and in opposite to metal utensils they
are nice and quiet to use.

#### Wooden Spoons don’t Conduct Heat
Another reason that wooden utensils are superior to metal utensils is that they
don’t conduct heat. If you leave a wooden spoon in the hot pot for a long time,
the handle will still stay cool. With a metal spoon, you’ll end up with a burned
hand. The plastic spoon will melt if it stirs something really hot or it rests
on the side of a hot pan.

#### Wood is an Inert Material
Another plus, wood is non-reactive and won’t leach harmful chemicals into your
food. Wood won’t react with the acids in foods and leave a metallic taste, like
some metal.

#### Easy to Grip
Holding metal, glass, ... can be uncomfortable to grip, while hard edges can
damage delicate ingredients. Wooden kitchen wears have gently rounded edges
and feel nice in the hand. You feel comfortable while using it.

#### Design
Another benefit of wooden kitchen wears is that they look fabulously beautiful
and elegant. The creative and artistic design of wood gives beautiful
appearance to the kitchen. Handcrafted wood kitchen wears are truly unique
functional works of art that make wonderful gifts.

#### Durability
Wood is very durable, and as long as you take proper care of them,
you can expect them to last a life time. They are hard to break. Hardwoods are
most sutable for kitchen use, and very hard to break. I would much prefer to
pick up the pieces of a wood bowl than glass! Since they can withstand heat,
wood kitchen utencles won’t melt, even in an unattended pan. A little sanding
from time to time can help remove burn marks and stains. Finish with a oil rub.
Use any oil that you would for cooking or salads, something with a long shelf
life that does not need refrigaration.

#### Germs and Bacteria don’t Like Wood
Research indicates that wood appears to have natural germ-killing properties. It
has been proven that germs and bacteria grow much faster on plastic and metal
than on wooden kitchenware surfaces.

#### Environmentally Responsible Choice
Environmentally-conscious individuals can choose wooden utensils with
confidence. Wood is natural and renewable resource and more environmentally
responsible choice. We can help save our environment by using kitchen utensils
that are made from renewable, biodegradable and non-toxic substances.

### What to look for, and how to care for wood kitchen wears.

Wood kitchen wear can last indefinitely – if it’s well cared for and maintained
regularly.

In fact, these organic containers can easily last long enough to eventually
become a family heirloom. And if the craftsman has created quality, hand
turned pieces, they may even become collectables.

Materials and technique used in the manufacturing process will have an impact on
the quality and durability of the product.

### They can be:
- Hand turned or machine turned on a lathe.
- Constructed of a single piece of wood or crafted out of wood segments
skillfully
glued together in layers, horizontally or vertically in a slat-like style.
- Made of laminate pieces, with a thin surface veneer of wood.

WOOD AND WATER DON’T MIX

To start, wooden bowls are made of wood.

After a tree has come down and is no longer growing, wood and water do not make
a good combination. Wood is a soft and porous material that will absorb water if
any is left sitting at the bottom of the vessel, or if the dish itself is
immersed in water.

When it absorbs water it will swell, sometimes resulting in cracks, twists or
splits. Hot water is particularly bad for this material.

This means, to preserve your bowl in the best possible condition, it should only
be wiped out with a damp cloth or given the briefest of washes in warm, soapy
water. Then it should be dried promptly with a soft tea towel.

To maintain the integrity of the wood, the container should be lightly rubbed
inside and out with mineral oil, or a healthy oil such as olive, sunflower or
coconut. Treat individual serving dishes and any wooden salad utensils in the
same manner.

THE BEST WOODS FOR BOWLS

Ideally, hardwoods are best suited for salad bowls. Hardwoods come from trees
with a broad leaf, that produce a nut or fruit, and that are found in temperate
regions. They lose their leaves in autumn and go dormant for the winter.

In the northern hemisphere these are deciduous trees, and in the sub-tropical
and tropical regions, they’re mostly evergreens.

As the name implies, hardwoods are generally harder than other types of wood,
and the density of their cellular structure makes them a more appropriate choice
for serving a dish with a high water content, such as salad.

Cherry – Cherry wood features the beautiful contrast between the rich, dark
heartwood of the tree’s inner section, and the light, creamy sapwood from the
outer part of the tree. Small bits of mineral deposits occur naturally
throughout the grain, adding little flecks of black highlights (or lowlights,
rather).

Cherry wood is known for its outstanding durability and the way its appearance
improves over time, taking on a lustre of deep, rich tones. And because of these
deep tones, it doesn’t need to be dyed or stained, so it’s 100% safe for serving
food.

Maple – Another durable and attractive wood that’s safe for use with food. The
pale color of maple gives it a light, countrified appearance that is charming
and honest. And characteristic of the top woods, it gets better with age as it
takes on a mature, lighter color and radiance.

Black Walnut – Another hardwood with a gorgeous contrast of light and dark
browns that requires no staining, making it a good choice for food. However,
with walnut there’s a hint of walnut fragrance and flavor in the bowl, a bonus
if you’re a fan of walnuts… And black walnut is one of the top rated hardwoods
for durability in today’s marketplace.

Teak – Dark wavy threads run throughout the grain, creating stunning highlights
in teak’s characteristic profile. A naturally water resistant hardwood from the
tropics, teak is very suitable in the kitchen. However, teak still requires the
usual light seasoning after use to protect it.

Olive – From the Mediterranean, olive wood is strong, heavy and dense and
features rich, earthy-brown streaks flowing with the lighter blond tones of the
sapwood and is known for the lustrous sheen it takes on with polishing. It too
may have a lingering, sweet aroma inherent in the wood.

Acacia – Strong and durable, and ranking high for hardness, acacia wood is water
and decay resistant as well. With its deep, rich ribbons of amber, gold and
earth tones in the lovely natural grain, it’s an excellent material for
kitchenware, and takes on a deep radiance when polished.

Bamboo – Not actually a tree, but a member of the grass family with wood-like
characteristics, bamboo grows at an amazingly quick rate. Easily sustainable,
strong and lightweight, it also produces anti-microbial properties that inhibit
the growth of bacteria.

Plants can be ready for harvest in just four years at a height of 60 – 70’. And
if you’re concerned about the ecological impact of dodgey harvest practices,
look for bamboo that has been FSC certified (Forest Stewardship Council) and
grown on plantations not used as a food source or habitat for the Giant Panda.

WOODS TO AVOID

Beech – Beech does not have the durable characteristics needed for kitchenware.
Without treatment, beech will yellow, fade and eventually turn gray in color.
This may be compensated for with staining or dying which may not be food safe.

Oak – Lovely to look at, oak examples just aren’t very durable. They’re easy to
chip and split, and also tend to shrink as they mature – and oak usually
requires a stain to bring out its features. However, staining that’s not FDA
approved may not be food safe and should be avoided.

You should especially avoid dishes and other products made from red oak – they
have an open and porous cell wall that allows moisture to pass through unlike
white oak (white oak used to be prized as the construction material for ships
due to its water resistant nature).

Birch – On the softer end of the hardwood scale, birch loses volume as it dries
making it prime material for twisting and warping; and as such, is not
appropriate for bowls.

BOWL CONSTRUCTION

ONE PIECE CONSTRUCTION

In the manufacturing process, vessels created from a single, solid piece of wood
that are hand turned on a lathe and kiln dried at a low heat will be the best
choice, structurally speaking. (Personal favorite)

SEGMENTED CONSTRUCTION

There are many options for beautifully crafted vessels made of segmented pieces,
glued together in a ring construction style or vertically joined in a slat-like
style.

But, with each seam and joint, the chance of water seepage increases. And even
if you take great care in cleaning and seasoning your bowl, water damage can
still occur.

LAMINATE CONSTRUCTION

A laminate is a thin slice of wood glued on top of another material such as a
softwood or composite material giving it a veneered appearance. Again, water can
enter the seams causing damage and lifting the laminate. I don’t recommend
these.

CLEANING AND CARE

Most wooden salad bowls can withstand a quick wash in warm soapy water, as long
as they’re inverted to drain and dried immediately. Never soak a wooden bowl, or
any wooden components, in water, don’t put them in the dishwasher.

Also, avoid rinsing them under running water, then setting them aside until
you’re ready to do the dishes – any water sitting in the bottom may cause
splitting.

Your salad bowl should be given a light seasoning of oil each time it’s washed;
and, include the exterior, individual servers and tongs or salad handles. Food
grade mineral oil is good, as are naturally healthy oils like olive, canola,
coconut etc.

However, use the mineral oil if you’re only going to use the bowl occasionally –
the others can go rancid and leave a foul taste.

Before creating your salad, season with lemon juice for its well-known
antibacterial properties and the crisp flavor it imparts. Rub the inside with
half a lemon, squeezing gently as you go for free flowing juice. Drain and
lightly absorb any excess droplets with a paper towel before adding your
ingredients.
