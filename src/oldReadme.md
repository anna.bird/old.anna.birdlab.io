## **_Eco City Living_**
**Planting a seed and going to watch it grow... spelling, grammar errors and
all. We are a small group looking to grow a tribe of tree planters around
the world. So we all have some shade and a place to hang a hammock.**
#### [Read More](index.md)


## **_Pilot Project_**
## **_"Eco Lodge"_** ~ George Town, Malaysia 
**A pilot project to "Eco City Living". Come stay with us and see what we 
are working on!**

### [Eco City Living Experience #2](https://www.airbnb.com/rooms/15553849) 
### [Eco City Living Experience #3](https://www.airbnb.com/rooms/15590733)

Currently we experiment with growing food in a urban setting. You'll find many 
moringa trees, aloe vera, lime trees and others, around the space. We also 
propagate cultures; kefir and kombucha. Now we begin to play with water! 
Reducing consumption and a full stop to polluting the water we use!

The "Eco Lodge": We consciously chose to be AC Free! Finding a place to stay 
that lent itself to this decision was not a quick task. But we eventually found 
it! Located in a low density well treed neighborhood. The "Eco Lodge " is 
located on the top (third) floor, of a wonderfully designed, low density 
apartment building. Facing north with large opening windows on three sides of the 
unit, allowing for a strong breeze to come through. Each room has opening windows 
(yes plural), and attached box for plants (natures air conditioners), large 
ceiling fan, special ordered and altered mosquito nets that are fully enclosed 
to ensure a peaceful nights sleep.

If you're a food lover and enjoy healthy living, or want to adopt this Lifestyle, 
look no further. We do our best to provide a healthy environment for your stay.

For more information about us and this project, or if you would like to get 
involved, please inquire.

## [Read More](index.md)
