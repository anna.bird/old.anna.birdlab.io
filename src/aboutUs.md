## In search of paradise.

**"Nature has an economy, an elegance, a style, that if we could but emulate,
we could rise out of the rubble we are making out of the planet."**

**With a life long fondness of nature, beauty and a seeking of the meraculous.
We
went on a long expidition, in search of Paradise. We never found it, so we
decided to create it.**

**"Art's task is to save the soul of mankind.. anything less is a dithering
while
_Rome burns_."**

![Eco Lodge Garden ~ Aloe Vera, plant of
immortality](./photo/ecoWebImages/ecoCityLivingAloe01.jpg)

**We feel a deep, burning responsibility to put all our energies into finding a
way for humankind to live in harmony with the enviroment, creating a living
enviroment that invests in the future, and limits the enviromental withdraw.**

![Eco Lodge
Garden](./photo/ecoWebImages/barefootEcoLodge.jpg)


**If you have something to contribute, or any questions, please contact us. #vivarium:matrix.org **

## [Services](services.md)
