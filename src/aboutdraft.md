As Eco Crusaders we Live ... . We organize and conduct workshops, co-creation
events for creative problem solving, handle business development, specialists in
online security and comunication, write and share information about Eco Living,
take care of social media management and PR, and a variety of other work.

We are currently setting up a eco active lifestyle brand: "EcoLifestyle"
Products made of recycled or sustainable materials, with a positive impact on
the enviroment and your suroundings. With money from sales coming in, we’re
investing in land restoration projects, and continuing our work with our
pilot project: Eco City Living

We are passionaly discovering and sharing solutions to everyday enviromental
impacts that we inflict on our emediate surroundings. Creating a healthy living
enviroment for you and your family, business ... . Turning the tables, from an
enviromental withdraw to a enviromental deposit for the future. It gives us a
thrill helping others to do the same.

For around five years now, we have been living a big part of our time out of a
bag. Diving into different communities fuels our inspiration and changing
surroundings sparks our creativity.

So here’s Eco Digital Nomad, and Eco City Living came to be. Besides working on
businesses and projects that will hopefully make Mother Nature smile, we hope to
inspire you to discover a new lifestyle. To not just live impact neutral, but to
make a positive one, while enjoying life as a professional wanderer.


Airbnb Text:

February special host. Eco-City Living is pleased to welcome Pascal.
Pascal is a talented adobe clay brick artist, who is eager to share and spread
the knowledge of this ancient craft. He will be your host for the month of
February.

Welcome to Our "Eco City Living" Pilot Project!

My roots are in hospitality, and include; owner/operator of a scuba diving lodge
and boutique style guesthouse. Simultaneously I also spent several years as
manager of a water taxi company located along the inside passage of British
Columbia, Canada, where I had the luxury of a captains position. My love of
nature was certainly satisfied by this work. My passions include ethnobotany,
and functional food, with formal education in herbalism. My husband has
a equally diverse background and now specializes in online security, and all
things Tech. We are two of the biggest nature lovers you will come across.

We are passionately discovering and sharing solutions to everyday environmental impacts that we inflict on our surroundings. Creating a healthy living environment for you, your family, business ... . Turning the tables, from an environmental withdraw to a environmental deposit for the future. It gives us a thrill helping others to do the same.

Currently we experiment with growing food in a urban setting. You'll find many
moringa trees, aloe vera, lime trees and others, around the space. We also
propagate cultures; kefir and kombucha. Now we begin to play with water!
Reducing consumption and a full stop to polluting the water we use!

We consciously chose to be AC Free! Finding a place to stay that lent itself to
this decision was not a quick task. But we eventually found it! Located in a low
density well treed neighborhood. Eco-City Living is located on the top (third)
floor, of a wonderfully designed, low density apartment building. Facing north
with large opening windows on three sides of the unit, allowing for a strong
breeze to come through. Each room has opening windows (yes plural), and attached box for plants (natures air-conditioners), large ceiling fan, special ordered and altered mosquito nets that are fully enclosed to ensure a peaceful nights sleep.       

If you're a food lover and enjoy healthy living, or want to adopt this
Lifestyle, look no further. We do our best to provide a healthy environment
for your stay.
