## Eco Living Guide: 11 Organizing Tips

Is your life in disarray? Do you have trouble finding things? Do you constantly
forget stuff? If so, don’t worry — you’re like most of the human population.

Disorganization is a natural state of order. Here are some tips to help get the
chaos of the universe into some form of organization.

If you have a desire to get organized, here are my favorite organization tips …
stuff I’ve learned along the way, from other sources and from experimentation.

So how do you do it? It’s simple. For everything that you own, designate a place
for it. You can even label some of those places, to make it easier to remember.
Find something that doesn’t have a place? Designate a place for it. Then simply
put things back where they belong when you’re done using them. It sounds simple,
but it actually takes a lot of practice to get this habit down.

So, before you bring any more into your life, first ask: Is it useful, does it
serve you, is it beautiful?

**“The good. What is it? Tricky, tricky, tricky. The true. What is it? Trickier,
even trickier. The beautiful. What is it? Easy to discern. The beautiful is easy
to discern. You are going to be condemned to live out the consequences of your
taste.”**

### Tips


1. Everything in its place. If you just followed one tip on this list, this
would be it. Practice this, and you can skip the rest of the tips. Seriously.

2. Start small. If your life and your home and your office are all incredibly
disorganized, don’t try to conquer all of it at once. Choose a small chunk
(maybe the top of your desk, or at least one part of it?) and organize that.
Then come up with a simple system to keep it organize, and try to practice that
system until it becomes habit. Now expand your “zone of organization” further,
to a new area. One chunk at a time, you can get organized.

3. Create routines. One of the best ways to keep your life organized is to make
routines for everything — for errands, laundry, finances, etc. And if you do
this one at a time, and make it a habit, you can optimize your life this way.

4. Clean as you go. This is a great habit … instead of having big cleaning
binges, clean things as you go. Done using some dishes? Wash them right away.
Clean the toilet when you finish using it, so it never gets dirty.

5. All info in one place. Use a text document, a wiki, a git, or some other type of
system to keep all the information you regularly use and need in one place. And get a password manager! Getting our digital lives more organized is key.
You’ll never look for a document, ... again. Single most huge time saver!

6. Put it away now. Done using something? Most people will put it down somewhere
nearby, with the intention of putting it away later. But messy houses and
offices are full of these intentions. Instead of letting things pile up, put
them away immediately. This inculdes your digital world! Right now, no exceptions.

7. Use an inbox, and empty it. Instead of having papers all over the place, have
one inbox for all incoming papers. Well, one for your office, and one for your
home. As Above for your didital "papers".

8. Keep a simple filing system. If your filing system is too complicated,
chances are you won’t use it. Use this simple system to keep your files in
order. Streamline everything!

9. Use calendar system, and todo list program to link your lives! Use alerts and notifications, this is your personal assistant #1.

10. Make your system usable. If you’re having problems with your organization
system, take a careful look at what’s tripping you up. Sometimes there’s a
difficulty or complication that makes the system too difficult to use. Maybe its time for a change.

11. Create a landing strip. When we get home, we empty our bag, plug in our computer and put the phone onto charge, then put
everything away or into a "safe place", this is a designated locaion with a designated recipient! This way it doesn’t get tossed on our counter
or table, and never have to look for it or forget where you leaft it.
