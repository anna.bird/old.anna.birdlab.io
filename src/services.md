## Eco City Living ~ Lifestyle

We offer private; personal, business cousultation and workshops in a variety
of enviroments, on a variety of topics.

Focused guidance, working together to organize your home, office and business
for improved efficiency of space and positive energy flow.

Business; from conception to launch. Proving instruction for online security
and comunication, eco design and footprint positive.

Eco City Living, nutrition programs for all lifestyles. Weight loss, paleo,
healthy lifestyle, vegitarian, ... . Personal, one on one tailored to your
needs. Intergrated fitness conculting is also avalable. We also offer corperate
nutrition consulting, providing guidance for healthy choices in the cafiteria.

![Eco City Living
Workshops](./photo/ecoWebImages/ecoLodgeKefirQA.jpg)

Party assistance needs, help you prepare for your party, healthy, nourishing
food and drinks menu, with medicinal properties, can include discusion on food
selection, nutritional influence, medicinal qualities and other intrests people
may have. We also assist with clean-up, including proper recycling and
minimizing waste. A discution of this can also be included. With a Q&A if
desired.

Follow up: We love to offer a follow up with all guests, clients and attendants,
if desired.
We value feedback on what we offer, always learning from eachother. Contact
with our partnerships is cherished and respected, in trust.

Gifts for your personal support. A combination of items we come across that meet
our personal standards are offered in our Shop, offered as a gift for your
support of our "Eco City Living" concept.

All our consultation services start with a free 15 minute conversation, you may
explore our principles before any comitment is made.

![Eco City LivingWorkshops](./photo/ecoWebImages/EcoLodgeFlowers.jpg)
