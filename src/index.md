## Eco City Living ~ _Emulate Nature_.

### Create your own Garden of Eden / Vivarium, at home.

**The question is, can we dream a dream that is sufficiently noble that we give
meaning to the sacrifices that have been made to allow the shopping malls, ...
of the
20th century to
exist …**

![Eco Lodge Garden](./photo/ecoWebImages/ecoCityLivingCoconutTree.jpg)

#### Eco City Living principles seek to:

- Minimise Ecological Footprints
- Maximise Human Potential (healthier living enviroment)
- Repair, replenish and support the processes that maintain life.

Eco City concept: reconstructing cities to be in balance with nature.
We bring this concept into your home. Creating a healthier living
enviroment, while reducing your ecological footprint to the level where your
actually creating a eviromental return, for the planet and your health.

#### Key Concepts of Eco City Living

Water: eliminating water contamination and reducing consumption. In house water
recycling, used for watering plants and household cleaning.

Waste: reducing kitchen waste through fermentation, compost and various other
methods. Often making healthier eating choices can drasticly reduce the waste
we accumulate. Most of the waste generated will be in a reused, or recycled
paking.
Striving to a near elimination of "garbage".

Power Consumption: Genorate power localy. Reduce power consumption through
healthier lighting choices. Growing plants to aid in reducing the need for
air-conditioners ... .

Home Furnishings: Natural fibers, furnishings made from recycled materials.
Multipurpose, functional, minamalism.

Transportation: Bicycle revival! Public transportation, ride sharing, ...

### Living in luxury, surounded by nature, in your own home.

![Eco Lodge
Garden](./photo/ecoWebImages/ecoCityLivingGotuKola.jpg)

## [About Us](aboutUs.md)
