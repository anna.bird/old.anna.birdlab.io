var metalsmith = require('metalsmith');
var markdown = require('metalsmith-markdown');

metalsmith(__dirname)
  .metadata({
    site: {
      name: 'Eco City Living',
      description: "Where the Dirt meets the Road"
    }
  })
  .source('./src')
  .destination('./public')
  .use(markdown())
  .build(function (err) {
    if (err) {
      console.log(err);
    }
    else {
      console.log('Site built!');
    }
  });
